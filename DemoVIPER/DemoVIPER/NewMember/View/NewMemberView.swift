//
//  NewMemberViewController.swift
//  DemoVIPER
//
//  Created by LeAnhDuc on 18/04/2022.
//

import UIKit

class NewMemberView: UIViewController, NewMemberViewProtocol {
    

    var presenter: NewMemberPresenterProtocol?
    
    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var ageLabel: UITextField!
    @IBOutlet weak var addNewButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func addNewAction(_ sender: Any) {
        
        if nameLabel.text != "" && ageLabel.text != ""{
            presenter?.addNewMember(member: Member(name: nameLabel.text!, age: Int(ageLabel.text!)!), from: self)
        }
        
    }
    
}
