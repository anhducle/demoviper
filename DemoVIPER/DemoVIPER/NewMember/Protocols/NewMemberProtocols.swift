//
//  NewMemberProtocols.swift
//  DemoVIPER
//
//  Created by LeAnhDuc on 18/04/2022.
//

import Foundation

protocol NewMemberViewProtocol{
    
    var presenter: NewMemberPresenterProtocol? { get set }
    
}

protocol NewMemberPresenterProtocol{
    
    var view: NewMemberViewProtocol? {get set}
    var wireframe: NewMemberWireframeProtocol? {get set}
    var callBack: SentBackMemberDataProtocol? {get set}
    
    func addNewMember(member: Member, from: NewMemberView)
    
    
}

protocol NewMemberWireframeProtocol{
    
    
    static func initialNewMemberModule(fromView: NewMemberView, inView: ListMemberView)
    func dismissViewAndAddNewMember(member: Member, from: NewMemberView)
}

protocol SentBackMemberDataProtocol{
    func onSentMemberData(member: Member)
}
