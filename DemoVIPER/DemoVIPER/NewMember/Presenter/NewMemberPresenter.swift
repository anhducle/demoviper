//
//  NewMemberPresenter.swift
//  DemoVIPER
//
//  Created by LeAnhDuc on 18/04/2022.
//

import Foundation
class NewMemberPresenter: NewMemberPresenterProtocol{
    var view: NewMemberViewProtocol?
    var callBack: SentBackMemberDataProtocol?
    var wireframe: NewMemberWireframeProtocol?
    
    func addNewMember(member: Member, from: NewMemberView) {
        wireframe?.dismissViewAndAddNewMember(member: member, from: from)
    }
    
    
    
}
