//
//  NewMemberWireframe.swift
//  DemoVIPER
//
//  Created by LeAnhDuc on 18/04/2022.
//

import Foundation

class NewMemberWireframe: NewMemberWireframeProtocol{
    
    
    class func initialNewMemberModule(fromView: NewMemberView, inView: ListMemberView) {
        let presenter = NewMemberPresenter()
        fromView.presenter = presenter
        fromView.presenter?.view = fromView
        fromView.presenter?.wireframe = NewMemberWireframe()
        fromView.presenter?.callBack = inView
    }
    
    func dismissViewAndAddNewMember(member: Member, from: NewMemberView) {
        from.presenter?.callBack?.onSentMemberData(member: member)
        from.navigationController?.popViewController(animated: true)
    }
    
}
