//
//  MemberInfoPresenter.swift
//  DemoVIPER
//
//  Created by LeAnhDuc on 15/04/2022.
//

import Foundation

class MemberInfoPresenter: MemberInfoPresenterProtocol{
    
    var view: MemberInfoViewProtocol?
    var member: Member?
    var wireframe: MemberInfoWireframeProtocol?
    
    func viewDidLoad() {
        view?.loadMemberData(member: member ?? Member(name: "no name", age: 0))
    }
    
}
