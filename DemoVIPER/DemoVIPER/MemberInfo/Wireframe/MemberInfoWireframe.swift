//
//  MemberInfoWireframe.swift
//  DemoVIPER
//
//  Created by LeAnhDuc on 15/04/2022.
//

import Foundation

class MemberInfoWireframe: MemberInfoWireframeProtocol{
    static func initialMemberInfoModule(fromView: MemberInfoView, member: Member) {
        let presenter = MemberInfoPresenter()
        
        presenter.member = member
        fromView.presenter = presenter
        fromView.presenter?.wireframe = MemberInfoWireframe()
        fromView.presenter?.view = fromView
        
    }
    
    
}
