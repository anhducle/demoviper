//
//  MemberInfo.swift
//  DemoVIPER
//
//  Created by LeAnhDuc on 15/04/2022.
//

import Foundation

protocol MemberInfoWireframeProtocol{
    
    static func initialMemberInfoModule(fromView: MemberInfoView, member: Member)
    
}

protocol MemberInfoPresenterProtocol{
    
    var view: MemberInfoViewProtocol? { get set }
    var wireframe: MemberInfoWireframeProtocol? { get set }
    
    func viewDidLoad()
}

protocol MemberInfoViewProtocol{
    
    var presenter: MemberInfoPresenterProtocol? { get set }
    
    
    
    func loadMemberData(member: Member)
}
