//
//  MemberInfoView.swift
//  DemoVIPER
//
//  Created by LeAnhDuc on 15/04/2022.
//

import UIKit

class MemberInfoView: UIViewController, MemberInfoViewProtocol {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    var presenter: MemberInfoPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter?.viewDidLoad()
        
    }
    
    func loadMemberData(member: Member) {
        nameLabel.text = member.name
        ageLabel.text = "\(member.age)"
    }
    
    
}
