//
//  ListTaskInteractor.swift
//  DemoVIPER
//
//  Created by LeAnhDuc on 15/04/2022.
//

import Foundation
class ListMemberInteractor: ListMemberInteractorInputProtocol{
    var presenter: ListMemberInteractorOutputProtocol?

    func getListMember() {
        presenter?.listMemberDidFetched(members: MemberData.getMemberData())
    }
    
    func addNewMemberIntoData(member: Member) {
        presenter?.listMemberDidFetched(members: MemberData.addMemberData(member: member))
    }
    
}
