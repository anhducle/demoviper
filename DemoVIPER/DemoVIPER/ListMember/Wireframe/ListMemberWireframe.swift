//
//  ListMemberWireframe.swift
//  DemoVIPER
//
//  Created by LeAnhDuc on 15/04/2022.
//

import Foundation
class ListMemberWireframe: ListMemberWireframeProtocol{
    class func initialMemberListModule(fromView: ListMemberView) {
        let presenter: ListMemberPresenterProtocol & ListMemberInteractorOutputProtocol = ListMemberPresenter()
        
        fromView.presenter = presenter
        fromView.presenter?.interactor = ListMemberInteractor()
        fromView.presenter?.wireframe = ListMemberWireframe()
        fromView.presenter?.view = fromView
        fromView.presenter?.interactor?.presenter = presenter
        
    }
    
    func pushToMemberInfo(from: ListMemberView, with: Member){
        let memberInfoViewController = from.storyboard?.instantiateViewController(withIdentifier: "memberinfo") as! MemberInfoView
        MemberInfoWireframe.initialMemberInfoModule(fromView: memberInfoViewController, member: with)
        from.navigationController?.pushViewController(memberInfoViewController, animated: true)
        
    }
    
    func pushToNewMember(from: ListMemberView) {
        let newMemberViewController = from.storyboard?.instantiateViewController(withIdentifier: "newmember") as! NewMemberView
        NewMemberWireframe.initialNewMemberModule(fromView: newMemberViewController, inView: from)
        from.navigationController?.pushViewController(newMemberViewController, animated: true)
    }
    
}
