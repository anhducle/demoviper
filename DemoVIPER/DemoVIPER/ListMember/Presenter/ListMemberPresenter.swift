//
//  ListMemberPresenter.swift
//  DemoVIPER
//
//  Created by LeAnhDuc on 15/04/2022.
//

import Foundation
class ListMemberPresenter: ListMemberPresenterProtocol, ListMemberInteractorOutputProtocol{
    
    var view: ListMemberViewProtocol?
    
    var interactor: ListMemberInteractorInputProtocol?
    
    var wireframe: ListMemberWireframeProtocol?
    
    func viewDidLoad() {
        interactor?.getListMember()
    }
    
    func listMemberDidFetched(members: [Member]) {
        view?.loadListMember(members: members)
    }
    
    func showMemberInfo(with: Member, from: ListMemberView) {
        wireframe?.pushToMemberInfo(from: from, with: with)
    }
    
    func showAddNewMemberView(from: ListMemberView) {
        wireframe?.pushToNewMember(from: from)
    }
    
    func addNewMember(member: Member) {
        interactor?.addNewMemberIntoData(member: member)
    }
    
}
