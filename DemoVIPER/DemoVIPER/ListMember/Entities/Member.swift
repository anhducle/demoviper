//
//  Member.swift
//  DemoVIPER
//
//  Created by LeAnhDuc on 15/04/2022.
//

import Foundation

struct Member {
    var name: String
    var age: Int
   
}
class MemberData {
    
    static var members = [Member(name: "Đức", age: 22),Member(name: "Nam", age: 20), Member(name: "Minh", age: 21), Member(name: "Hùng", age: 25)]
    
    class func getMemberData() -> [Member] {
        return members
    }
    
    class func addMemberData(member: Member) -> [Member]{
        members.append(member)
        return members
    }
    
}
