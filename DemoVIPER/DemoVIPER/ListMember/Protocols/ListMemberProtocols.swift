//
//  ListTaskProtocol.swift
//  DemoVIPER
//
//  Created by LeAnhDuc on 15/04/2022.
//

import Foundation

protocol ListMemberViewProtocol{
    var presenter: ListMemberPresenterProtocol? { get set }
    
    func loadListMember(members: [Member])
    
}

protocol ListMemberInteractorInputProtocol{
    var presenter: ListMemberInteractorOutputProtocol? {get set}
    
    func getListMember()
    func addNewMemberIntoData(member: Member)
    
}

protocol ListMemberInteractorOutputProtocol{
    func listMemberDidFetched(members: [Member])
}

protocol ListMemberPresenterProtocol{
    var view: ListMemberViewProtocol? {get set}
    var interactor: ListMemberInteractorInputProtocol? {get set}
    var wireframe: ListMemberWireframeProtocol? {get set}
    
    
    func viewDidLoad() // request load data in the first time load view
    func showMemberInfo(with: Member, from: ListMemberView)// request to show info selected member
    func showAddNewMemberView(from: ListMemberView)//request to push to add new member view
    func addNewMember(member: Member)//add new member into data
}

protocol ListMemberWireframeProtocol{
    
    func pushToMemberInfo(from: ListMemberView, with: Member)
    func pushToNewMember(from: ListMemberView)
    static func initialMemberListModule(fromView: ListMemberView)
}




