//
//  ListTaskView.swift
//  DemoVIPER
//
//  Created by LeAnhDuc on 15/04/2022.
//

import UIKit

class ListMemberView: UIViewController, ListMemberViewProtocol, SentBackMemberDataProtocol {
    
    @IBOutlet weak var listMemberTableView: UITableView!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    var members = [Member]()
    
    var presenter: ListMemberPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ListMemberWireframe.initialMemberListModule(fromView: self)
        presenter?.viewDidLoad()
    }
    
    func loadListMember(members: [Member]) {
        self.members = members
        listMemberTableView.reloadData()
    }
    @IBAction func addAction(_ sender: Any) {
        presenter?.showAddNewMemberView(from: self)
    }
    
    func onSentMemberData(member: Member) {
        presenter?.addNewMember(member: member)
    }
    
}
extension ListMemberView: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ListMemberTableViewCell else { return UITableViewCell() }
        cell.nameLabel.text = members[indexPath.row].name
        cell.ageLabel.text = "\(members[indexPath.row].age)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.showMemberInfo(with: members[indexPath.row], from: self)
    }
    
    
}
